var configs = require('./configs.js');

var db = require('./utils/db');
db.init(configs.mysql());

// client_service.start(configHall);

const express = require('express');
const bodyParser = require('body-parser');

let server = new express();
const wxconfig = configs.wxconfig();


server.listen(wxconfig.port);

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: false}));


const destination = require('./route/destination');
const index = require('./route/index');
const trips = require('./route/trips');
const users = require('./route/users')
const login = require('./route/login');

server.use('/destination',destination);
server.use('/index',index);
server.use('/trips',trips);
server.use('/users',users);
server.use('/login',login);




